class Usuario < ApplicationRecord
  has_secure_password
  belongs_to :rol
end
