class Api::V1::UsuariosController < ApplicationController
    load_and_authorize_resource class: "Usuario"

    def index
        usuarios = Usuario.all
        render json: usuarios, status: :ok
    end

    def create
        usuario = Usuario.new(usuario_params)
        if usuario.save
            render json: usuario, status: :ok
        else
            render json: "error", status: :unprocessable_entity
        end
    end

    def show
        usuario = Usuario.find(params[:id])
        render json: usuario, status: :ok
    end

    private
    def usuario_params
        params.permit(:correo, :password, :rol_id)
    end
end