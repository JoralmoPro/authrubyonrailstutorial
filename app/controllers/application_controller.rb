class ApplicationController < ActionController::Base
    before_action :set_locale
    protect_from_forgery with: :null_session
    include ManejadorDeExcepciones

    rescue_from CanCan::AccessDenied do |exception|
        render json: { mensaje: exception.message }, status: 403
    end

    def current_user
        if token
            @usuario_actual ||= Usuario.find(token[:usuario_id])
        else
            @usuario_actual ||= Usuario.new(rol_id: 3)
        end
    end

    private
    def token
        valor = request.headers[:Authorization]
        return if valor.blank?
        @token ||= JsonWebToken.decode(valor.split(" ").last)
    end
    def set_locale
        I18n.locale = "es"
    end
end
