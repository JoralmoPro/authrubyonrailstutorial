Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :usuarios
      post 'login', to: 'session#login'
    end
  end
end
