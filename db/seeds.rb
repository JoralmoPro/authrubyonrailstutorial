# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
administrador = Rol.create(nombre: "administrador")
cliente = Rol.create(nombre: "cliente")
visitante = Rol.create(nombre: "visitante")

Usuario.create(correo: "joralmopro@gmail.com", password: "123456", rol: administrador)

5.times do
    usuario = Usuario.create(correo: Faker::Internet.email, password: "cliente", rol: cliente)
    Post.create(titulo: Faker::Book.title, contenido: Faker::Lorem.paragraph, usuario: usuario)
end

p "#{Usuario.count} usuarios creados"
p "#{Rol.count} roles creados"
p "#{Post.count} post creados"