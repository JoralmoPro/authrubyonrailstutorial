class CreateUsuarios < ActiveRecord::Migration[5.2]
  def change
    create_table :usuarios do |t|
      t.string :correo
      t.string :password_digest
      t.references :rol, foreign_key: true

      t.timestamps
    end
  end
end
